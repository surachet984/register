import { IsEmail, IsPhoneNumber, IsString, Length } from "class-validator";

export class UserDto {
    @IsString()
    fullNameTh: string;

    @IsString()
    fullNameEng: string;

    @Length(10)
    telephone: string;

    @IsEmail({}, { message: "กรุณากรอก email ให้ถูกต้อง" })
    email: string;

    @IsString()
    password: string;
}