import { BaseEntity } from 'src/base/base.entity';
import { Column, Entity, Generated, In, Index, JoinTable, ManyToMany, ManyToOne, OneToMany } from 'typeorm';


@Entity()
export class User extends BaseEntity {
    constructor() {
        super();
    }

    @Column()
    fullNameTh: string;

    @Column()
    fullNameEng: string;

    @Column({ nullable: true ,length:10})
    telephone: string;

    @Column()
    email: string;

    @Column()
    password: string;

    @Column({ default: false })
    isEmailVerified: boolean;

    @Column({ nullable: true })
    emailVerificationToken: string;

}
