import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { User } from './entity/user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UserDto } from './dto/user.dto';
import { MESSAGE } from 'src/constant/message';
import * as bcrypt from 'bcrypt';
import { v4 as uuidv4 } from 'uuid';
import { EmailService } from 'src/email/email.service';

@Injectable()
export class UserService {
    constructor(
        @InjectRepository(User)
        private readonly userRepository: Repository<User>,
        private readonly emailService: EmailService,
    ) { }


    async register(userDto: UserDto) {
        try {
            const findUserByEmail = await this.userRepository.findOne({ where: { email: userDto?.email } })

            if (!findUserByEmail) {
                let password = await this.hashPassword(userDto?.password)
                let dataUser = await this.userRepository.create({ ...userDto, password, emailVerificationToken: uuidv4() })
                let saveUser = await this.userRepository.save(dataUser)

                if (saveUser) {
                    await this.emailService.sendVerificationEmail(saveUser.email, saveUser.emailVerificationToken);
                } else {
                    throw new HttpException(MESSAGE.UNABLE_COMPLETE_TRANSACTION, HttpStatus.FORBIDDEN);
                }

            } else {
                throw new HttpException(MESSAGE.DUPLICATE_EMAIL, HttpStatus.FORBIDDEN);
            }
        } catch (error) {
            throw new HttpException(error?.message, HttpStatus.BAD_REQUEST);
        }
    }

    async verificationToken(id: string) {
        try {
            const findUserByToekn = await this.userRepository.findOne({
                where: {
                    emailVerificationToken: id,
                }
            })

            if (findUserByToekn) {
                if (!findUserByToekn?.isEmailVerified) {
                    let updateIsEmailVerified = await this.userRepository.update({ id: findUserByToekn?.id }, { isEmailVerified: true })
                    return updateIsEmailVerified ? "ยืนยัน Email สำเร็จ" : "ไม่สามารถทำรายการได้"
                } else {
                    return "Email นี้ได้ถูกยืนยันตนแล้ว"
                }
            } else {
                return "ไม่สามารถทำรายการได้"
            }
        } catch (error) {
            throw new HttpException(error?.message, HttpStatus.BAD_REQUEST);
        }
    }

    async hashPassword(password: string) {
        const salt = await bcrypt.genSalt();
        const hashPassword = await bcrypt.hash(password, salt);
        return hashPassword
    }

}
