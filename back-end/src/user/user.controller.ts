import { Body, Controller, Get, Param, Patch, Post } from '@nestjs/common';
import { UserService } from './user.service';
import { UserDto } from './dto/user.dto';

@Controller('user')
export class UserController {
    constructor(private userService: UserService) { }

    @Get("verification-token/:id")
    async verificationToken(@Param("id") id: string) {
        return await this.userService.verificationToken(id)
    }

    @Post("register")
    async register(@Body() userDto: UserDto) {
        return await this.userService.register(userDto)
    }

}
