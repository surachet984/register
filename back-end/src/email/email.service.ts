import { Injectable } from '@nestjs/common';
import * as nodemailer from 'nodemailer';

@Injectable()
export class EmailService {
    private transporter;

    constructor() {
        this.transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: {
                user: process.env.EMAIL_SENDER,
                pass: process.env.APP_PASSWORD
            }
        });
    }

    async sendVerificationEmail(email: string, token: string): Promise<void> {
        const mailOptions = {
            from: 'surachet.ch1997@gmail.com',
            to: email,
            subject: 'ยืนยัน Email',
            text: `กด Link ยืนยัน Email : http://127.0.0.1:8000/user/verification-token/${token}`,
        };

        await this.transporter.sendMail(mailOptions);
    }
}
