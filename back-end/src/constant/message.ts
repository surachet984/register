export const MESSAGE = {
    UNABLE_COMPLETE_TRANSACTION: "ไม่สามารถทำรายการได้",
    DUPLICATE_EMAIL: "อีเมลนี้ถูกใช้งานแล้ว",
    UNABLE_TP_SEND_EMAIL:"ไม่สามารถส่งอีเมลได้"
}
